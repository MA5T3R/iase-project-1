//Global variables
var pawnSet = false;
var position = "";
var table = Create2DArray(8);
var myPawn = null;
var myShadow = null;
var goalFound = false;
var numberOfNodes = 8;
var optionSelected = null;

//Custom function used for creating matrix
function Create2DArray(rows) {
    var arr = [];
    for (var i = 0; i < rows; i++) {
        arr[i] = [];
    }
    return arr;
}

//Initialization of chess table and each cell
window.onload = function initialization() {
    console.log("Initialization Chess Table");
    for (var i = 0; i < numberOfNodes; i++) {
        for (var j = 0; j < numberOfNodes; j++) {
            var div = document.getElementById("mainChessBoard").appendChild(document.createElement("div"));
            table[i][j] = new cell(i, j);
            div.style.backgroundColor = parseInt(i + j) % 2 == 0 ? '#ababab' : 'white';
            div.setAttribute("class", "cell");
            div.setAttribute("id", table[i][j].getStep());
            div.setAttribute("onclick", "movement(this)");
        }
    }
}

//Cell object: definition of each cell in part - add any particular element if needed
var cell = function (i, j) {
    var empty = true;
    var pastStep = false;
    var x = i;
    var y = j;
    this.getStep = function () {
        var string = "" + x + "x" + y;
        return string;
    };
    this.isPastStep = function () {
        return pastStep;
    };
    this.setPastStep = function () {
        pastStep = true;
    };
    this.removePastStep = function () {
        pastStep = false;
    };
    this.isEmpty = function () {
        empty = true;
    };
    this.isOccupied = function () {
        empty = false;
    };
    this.state = function () {
        return empty;
    };
}

$(function () {
    $('#cd-dropdown').dropdown({
        gutter: 5,
        stack: false,
        slidingIn: 100,
        minus: 61
    });
    optionSelected = $(".cd-dropdown span");
});

//Event function: functionality depending on the chosen option out of the combo box
function movement(obj) {
    obj = $(obj);
    var currentPosition = obj.attr('id').split("x");
    switch (optionSelected.first().text()) {
    case 'Define placement':
        console.log("Define movement." + obj.attr("id"));
        if (pawnSet == false) {
            console.log("Dfine 1st position.");
            position = obj.attr('id');
            myObj = document.getElementById("0x0");
            myObj.appendChild(document.createElement("span")).setAttribute("id", "locator");
            myObj.appendChild(document.createElement("span")).setAttribute("id", "pawn");
            myPawn = $('#pawn');
            myShadow = $('#locator');
            changePosition(currentPosition[0], currentPosition[1], myPawn, 200);
            pawnSet = true;
            table[currentPosition[0]][currentPosition[1]].isOccupied();
        } else {
            var i = parseInt(position.split("x")[0]);
            var j = parseInt(position.split("x")[1]);
            if (i != currentPosition[0] || j != currentPosition[1]) {
                console.log("Change position from: " + position + " to " + currentPosition[0] + "x" + currentPosition[1]);
                if (i < currentPosition[0] || (i == currentPosition[0] && j < currentPosition[1])) {
                    var reverse = false;
                } else {
                    var reverse = true;
                }
                console.log("Reverse value:" + reverse);
                while (i != currentPosition[0] || j != currentPosition[1]) {
                    table[i][j].isEmpty();
                    if (reverse == false) {
                        if (j + 1 < numberOfNodes)
                            j++;
                        else {
                            i < currentPosition[0] ? i++ : i--;
                            j = numberOfNodes - 1;
                            reverse = true;
                        }
                    } else {
                        if (j - 1 >= 0)
                            j--;
                        else {
                            i < currentPosition[0] ? i++ : i--;
                            j = 0;
                            reverse = false;
                        }
                    }
                    changePosition(i, j, myPawn, 200);
                    table[i][j].isOccupied();
                }
                position = obj.attr('id');
            } else {
                console.log("No change.");
            }
        }
        break;
    case 'Breadth-First Search':
        if (pawnSet != false) {
            console.log("---Breadth-First Search: started---");
            changePosition(currentPosition[0], currentPosition[1], myShadow, 0);
            resetTableSteps();
            myShadow.css("visibility", "visible");
            try {
                breadthFirstSearch(parseInt(currentPosition[0]), parseInt(currentPosition[1]));
            } catch (msg) {
                console.log("---Pawn found at haha" + msg + "---");
                outputLog("Pawn found at " + msg);
            }
            myShadow.queue(function () {
                $(this).css("visibility", "hidden");
                $(this).dequeue();
            });
            console.log("---Breadth-First Search: finished---");
        } else {
            console.log("Please define pawn placement!");
            outputLog("Please define pawn placement!");
        }
        break;
    case 'Depth-limited Search':
        if (pawnSet != false) {
            console.log("---Depth-limited Search: started---");
            changePosition(currentPosition[0], currentPosition[1], myShadow, 0);
            resetTableSteps();
            myShadow.css("visibility", "visible");
            try {
                depthLimitedSearch(parseInt(currentPosition[0]), parseInt(currentPosition[1]),
                    0, 6);
            } catch (msg) {
                console.log(msg);
                outputLog(msg);
            }
            myShadow.queue(function () {
                $(this).css("visibility", "hidden");
                $(this).dequeue();
            });
            console.log("---Depth-limited Search: finished---");
        } else {
            console.log("Please define pawn placement!");
            outputLog("Please define pawn placement!");
        }
        break;
    case 'Iterative Deepening Search':
        if (pawnSet != false) {
            console.log("---Iterative Deepening Search: started---");
            changePosition(currentPosition[0], currentPosition[1], myShadow, 0);
            resetTableSteps();
            myShadow.css("visibility", "visible");
            iterativeDeeping(parseInt(currentPosition[0]), parseInt(currentPosition[1]));
            myShadow.queue(function () {
                $(this).css("visibility", "hidden");
                $(this).dequeue();
            });
            console.log("---Iterative Deepening Search: finished---");
        } else {
            console.log("Please define pawn placement!");
            outputLog("Please define pawn placement!");
        }
        break;
    case 'Greedy Search':
        if (pawnSet != false) {
            console.log("---Greedy Search: started---");
            changePosition(currentPosition[0], currentPosition[1], myShadow, 0);
            resetTableSteps();
            myShadow.css("visibility", "visible");
            try {
                greedySearch(parseInt(currentPosition[0]), parseInt(currentPosition[1]));
            } catch (msg) {
                console.log("---Pawn found at " + msg + "---");
                outputLog("Pawn found at " + msg);
            }
            myShadow.queue(function () {
                $(this).css("visibility", "hidden");
                $(this).dequeue();
            });
            console.log("---Greedy Search: finished---");
        } else {
            console.log("Please define pawn placement!");
            outputLog("Please define pawn placement!");
        }
        break;
    default:
        console.log("No option selected!");
        outputLog("No option selected!");
    }

}

//Change position to [i][j] based on matrix
//http://ricostacruz.com/jquery.transit
function changePosition(i, j, obj, speed) {
    console.log("Moving pawn to:" + i + "x" + j);
    var destination = $("#" + table[i][j].getStep()).position();
    if (pawnSet) {
        obj.transition({
            x: destination.left - 11,
            y: destination.top - 3,
            duration: speed
        });
    } else {
        obj.css("x", destination.left - 11).css("y", destination.top - 3).css("visibility", "visible");
    }
}

function resetTableSteps() {
    for (var i = 0; i < numberOfNodes; i++)
        for (var j = 0; j < numberOfNodes; j++)
            table[i][j].removePastStep();
}

/* 1.Breadth-First Search - Iasmina
for each node n in Graph:
       n.distance = INFINITY
       n.parent = NIL

      create empty queue Q

      root.distance = 0
      Q.enqueue(root)

      while Q is not empty:

      current = Q.dequeue()

     for each node n that is adjacent to current:
       if n.distance == INFINITY:
           n.distance = current.distance + 1
           n.parent = current
           Q.enqueue(n)
 */
function breadthFirstSearch(i, j) {
    if (i >= numberOfNodes || i < 0 || j >= numberOfNodes || j < 0) {
        return;
    } else {
        if (!table[i][j].isPastStep()) {
            table[i][j].setPastStep();
            changePosition(i, j, myShadow, 150);
            if (!table[i][j].state()) {
                throw i + "x" + j;
            }
        }
        myPawn_x = position[0];
        myPawn_y = position[2];
        if (position[0] == i && position[2] < j) {
            breadthFirstSearch(i, j - 1);
        } else if (position[0] < i) {
            breadthFirstSearch(i, j - 1);
            var l = 7;
            breadthFirstSearch(i - 1, l);
            breadthFirstSearch(i, l + 1);
        } else {
            breadthFirstSearch(i, j + 1);
            var l = 0;
            breadthFirstSearch(i + 1, l);
            breadthFirstSearch(i, l + 1);
        }

    }
}


/* 4.Depth-limited Search
https://en.wikipedia.org/wiki/Depth-limited_search
----------Pseudocode----------
DLS(node, goal, depth) {
  if ( depth >= 0 ) {
    if ( node == goal )
     x=goal if ( goal=depth )
       return node
    for each child in expand(node)
      DLS(child, goal, depth-1)
  }
}
*/
function depthLimitedSearch(i, j, depth, limit) {
    if (depth <= limit) {
        if (i >= numberOfNodes || i < 0 || j >= numberOfNodes || j < 0) {
            return;
        } else {
            if (!table[i][j].isPastStep()) {
                console.log("Depth:" + depth);
                table[i][j].setPastStep();
                changePosition(i, j, myShadow, 150);
                if (!table[i][j].state()) {
                    throw "Goal Found at depth " + depth + ", having the following position: " + i + "-" + j;
                }
            }
            depth++;
            depthLimitedSearch(i + 1, j, depth, limit);
            depthLimitedSearch(i + 1, j + 1, depth, limit);
            depthLimitedSearch(i, j + 1, depth, limit);
            depthLimitedSearch(i - 1, j + 1, depth, limit);
            depthLimitedSearch(i - 1, j, depth, limit);
            depthLimitedSearch(i - 1, j - 1, depth, limit);
            depthLimitedSearch(i, j - 1, depth, limit);
            depthLimitedSearch(i + 1, j - 1, depth, limit);
        }
    }
}

/* 5.Iterative Deepening Search
https://en.wikipedia.org/wiki/Iterative_deepening_depth-first_search
----------Pseudocode----------
procedure IDDFS(root)
    for depth from 0 to ∞
        found ← DLS(root, depth)
        if found ≠ null
            return found
 */
function iterativeDeeping(i, j) {
    var depth = 0
    try {
        for (; depth < numberOfNodes; depth++) {
            depthLimitedSearch(i, j, 0, depth);
        }
    } catch (msg) {
        console.log(msg);
        outputLog(msg);
    }
}

/* 6.Greedy Search ? Iasmina to choose - HAHA

*/
function greedySearch(i, j) {
    if (i >= numberOfNodes || i < 0 || j >= numberOfNodes || j < 0) {
        return;
    } else {
        if (!table[i][j].isPastStep()) {
            table[i][j].setPastStep();
            changePosition(i, j, myShadow, 150);
            if (!table[i][j].state()) {
                throw i + "x" + j;
            }
        }

        var m = position[0] - i;
        var n = position[2] - j;
        /*cadran4*/
        if(i > position[0] && j > position[2] && m>=n){
            changePosition(i+m, j+m, myShadow, 150);
            changePosition(i+m, j+n, myShadow, 150);
        }
        if(i > position[0] && j > position[2] && m<n){
            changePosition(i+n, j+n, myShadow, 150);
            changePosition(i+m, j+n, myShadow, 150);
        }
        /*cadran3*/
        if(i > position[0] && j < position[2] && m>=-n){
            changePosition(i+m, j-m, myShadow, 150);
            changePosition(i+m, j+n, myShadow, 150);
        }
        if(i > position[0] && j < position[2] && m<-n){
            changePosition(i-n, j+n, myShadow, 150);
            changePosition(i+m, j+n, myShadow, 150);
        }
        /*egalitate*/
        if(i == position[0] || j == position[2]){
            changePosition(i+m, j+n, myShadow, 150);
        }
        /*cadran2*/
        if(i < position[0] && j < position[2] && m<=n){
            changePosition(i+m, j+m, myShadow, 150);
            changePosition(i+m, j+n, myShadow, 150);
        }
        if(i < position[0] && j < position[2] && m>n){
            changePosition(i+n, j+n, myShadow, 150);
            changePosition(i+m, j+n, myShadow, 150);
        }
        /*cadran1*/
        if(i < position[0] && j > position[2] && m<-n){
            changePosition(i+m, j-m, myShadow, 150);
            changePosition(i+m, j+n, myShadow, 150);
        }
        if(i < position[0] && j > position[2] && m>=-n){
            changePosition(i-n, j+n, myShadow, 150);
            changePosition(i+m, j+n, myShadow, 150);
        }


    }
}

function outputLog(string) {
    $("#consoleOutput").text(string);
}
